# README #


## Play by Play: iOS and Swift from Scratch
by Brian Clark and John Papa

Learn the fundamentals of developing with iOS using Swift and Xcode by building an app to track your favorite movies. The code to follow along with this course is available at github.com/clarkio/ios-favorite-movies.

## Pluralsite
https://app.pluralsight.com/library/courses/play-by-play-ios-swift-from-scratch/table-of-contents