//
//  MovieDataProcessor.swift
//  favorite-movies-app
//
//  Created by Douglas De Voogt on 8/31/17.
//  Copyright © 2017 Douglas. All rights reserved.
//

import Foundation

class MovieDataProcessor {
    static func mapJsonToMovies(object: [String: AnyObject], moviesKey: String) -> [Movie] {
        var mappedMovies: [Movie] = []
        
        guard let movies = object[moviesKey] as? [[String: AnyObject]] else {return mappedMovies}
        
        for movie in movies {
            guard let id = movie["id"] as? Int,
                let name = movie["original_title"] as? String,
                let year = movie["release_date"] as? String,
                let imageUrlPart = movie["poster_path"] as? String else {continue}
            
            let imageUrl = "https://image.tmdb.org/t/p/w92\(imageUrlPart)"
            
            let movieClass = Movie(id: id, title: name, year: year, imageUrl: imageUrl)
            mappedMovies.append(movieClass)
        }
        return mappedMovies
    }
}
