//
//  RegexHelper.swift
//  favorite-movies-app
//
//  Created by Douglas De Voogt on 9/1/17.
//  Copyright © 2017 Douglas. All rights reserved.
//

import Foundation

class RegexHelper {
    static func firstMatchingString(pattern: String, text: String) -> String? {
        
        let nsStringText = text as NSString
        
        let regex = try! NSRegularExpression(pattern: pattern, options: [])
        
        let results = regex.matches(in: text, options: [], range: NSRange(location: 0, length: nsStringText.length))
        
        var matchingString: String?
        
        if !results.isEmpty {
            matchingString = nsStringText.substring(with: results[0].range)
        }
        
        return matchingString
    }
}
