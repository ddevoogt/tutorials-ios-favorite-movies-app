//
//  JsonParser.swift
//  favorite-movies-app
//
//  Created by Douglas De Voogt on 8/31/17.
//  Copyright © 2017 Douglas. All rights reserved.
//

import Foundation

class JsonParser {
    static func parse (data: Data) -> [String: AnyObject]? {
        let options = JSONSerialization.ReadingOptions()
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: options) as? [String: AnyObject]
            return json!
        } catch (let parseError){
            print("There was an error parsing the JSON: \"\(parseError.localizedDescription)\"")
        }
        return nil
    }
}
