//
//  Movie.swift
//  favorite-movies-app
//
//  Created by Douglas De Voogt on 8/30/17.
//  Copyright © 2017 Douglas. All rights reserved.
//

import Foundation

class Movie {
    var id: Int = -1
    var title: String = ""
    var year: String = ""
    var imageUrl: String = ""
    var plot: String = ""
    
    init(id: Int, title: String, year: String, imageUrl: String, plot: String = "") {
        self.id = id
        self.title = title
        self.year = getJustYear(stringWithYear: year)
        self.imageUrl = imageUrl
        self.plot = plot
    }
    
    private func getJustYear(stringWithYear: String?) -> String {
        guard !(stringWithYear ?? "").isEmpty else {
            return ""
        }
        let pattern = "([1-2][0-9]{3})"
        return RegexHelper.firstMatchingString(pattern: pattern, text: stringWithYear!) ?? stringWithYear!
    }
}
